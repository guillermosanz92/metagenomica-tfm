# Metagenomica TFM

Es el resultado de mi TFM. Consiste en el analisis de datos de metagenomica del lago hipersalino de Petrola 

## Archivos importantes

[TFM pdf](TFM_Guillermo_Sanz.pdf)    

[Codigo utilizado python (Qiime2)](1_script qiime2.ipynb)   

[Resumen de los datos obtenidos de diversidad](2_Diversidad_alfa_y_beta_y_analisis_diferencial.html) <- Tarda en cargar pero merece la pena
